# Source global definitions
if [ -f /etc/bashrc ]; then
	. /etc/bashrc
fi

# Override default Fedora prompt modifications
declare -a PROMPT_COMMAND=(":")
PS0=""

SHELL="bash"
if [[ -f "/usr/bin/bash" ]]; then
    SHELL="/usr/bin/bash"
elif [[ -n "$(readlink -f "/proc/$$/exe")" ]]; then
    SHELL="$(readlink -f "/proc/$$/exe")"
fi
export SHELL

# User-specific aliases and functions
source "$HOME/.config/shell-profile/profile.sh"
