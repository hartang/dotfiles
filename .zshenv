SHELL="zsh"
if [[ -f "/usr/bin/zsh" ]]; then
    SHELL="/usr/bin/zsh"
elif [[ -n "$(readlink -f "/proc/$$/exe")" ]]; then
    SHELL="$(readlink -f "/proc/$$/exe")"
fi
export SHELL

export ZDOTDIR="$HOME/.config/zsh"
