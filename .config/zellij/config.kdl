plugins {
    tab-bar location="zellij:tab-bar"
    status-bar location="zellij:status-bar"
    strider location="zellij:strider"
    compact-bar location="zellij:compact-bar"
    session-manager location="zellij:session-manager"
    welcome-screen location="zellij:session-manager" {
        welcome_screen true
    }
    filepicker location="zellij:strider" {
        cwd "/"
    }
    tabline location="https://gitlab.com/api/v4/projects/53442724/packages/generic/release/0.2.0/tabline.zj" {
        style_tab "fg=#717CB4 + italic"
        is_selectable "false"
    }
}

ui {
    pane_frames {
        rounded_corners true
    }
}

themes {
    default {
        fg 251          // light gray
        bg 251          // light gray --> unused?
        white 251       // light gray
        yellow 220      // yellow --> unused?
        orange 75       // magenta
        red 167         // red
        magenta 135     // magenta
        blue 75         // blue --> unused?
        cyan 14         // cyan --> unused?
        green 214       // orange
        black "#212121" // dark gray
    }

    alt {
        fg 236
        bg 251
        white 236
        yellow 3
        orange 214
        red 196
        magenta 206
        blue 4
        cyan 14
        green 206
        black 214
    }
}

keybinds clear-defaults=true {
    shared_among "normal" "pane" "tab" "resize" "move" "scroll" "search" "session" {
        bind "F1" { SwitchToMode "locked"; }
        bind "F2" { SwitchToMode "pane"; }
        bind "F3" { SwitchToMode "tab"; }
        bind "F4" { SwitchToMode "resize"; }
        bind "F5" { SwitchToMode "move"; }
        bind "F6" { SwitchToMode "scroll"; }
        bind "F7" { SwitchToMode "session"; }
        bind "F8" { Quit; }
    }
    shared_except "locked" {
        bind "Alt n" { NewPane; }
        bind "Alt h" { MoveFocus "Left"; }
        bind "Alt j" { MoveFocus "Down"; }
        bind "Alt k" { MoveFocus "Up"; }
        bind "Alt l" { MoveFocus "Right"; }
        bind "Alt +" { Resize "Increase"; }
        bind "Alt -" { Resize "Decrease"; }
    }
    locked {
        bind "F1" { SwitchToMode "normal"; }
    }
    pane {
        bind "h" "Left" { MoveFocus "Left"; }
        bind "j" "Down" { MoveFocus "Down"; }
        bind "k" "Up" { MoveFocus "Up"; }
        bind "l" "Right" { MoveFocus "Right"; }
        bind "n" { NewPane; SwitchToMode "normal"; }
        bind "x" { CloseFocus; SwitchToMode "normal"; }
        bind "c" { SwitchToMode "RenamePane"; PaneNameInput 0; }
        bind "d" { NewPane "Down"; SwitchToMode "normal"; }
        bind "r" { NewPane "Right"; SwitchToMode "normal"; }
        bind "f" { ToggleFocusFullscreen; SwitchToMode "normal"; }
        bind "z" { TogglePaneFrames; SwitchToMode "normal"; }
        bind "w" { ToggleFloatingPanes; SwitchToMode "normal"; }
        bind "e" { TogglePaneEmbedOrFloating; SwitchToMode "normal"; }
        bind "p" { SwitchFocus; }
        bind "[" { PreviousSwapLayout; }
        bind "]" { NextSwapLayout; }

        bind "F2" "Enter" "Space" "Esc" { SwitchToMode "normal"; }
    }
    tab {
        bind "h" "Left" { GoToPreviousTab; }
        bind "H" { MoveTab "Left"; }
        bind "l" "Right" { GoToNextTab; }
        bind "L" { MoveTab "Right"; }
        bind "n" { NewTab; SwitchToMode "normal"; }
        bind "x" { CloseTab; SwitchToMode "normal"; }
        bind "c" { SwitchToMode "RenameTab"; TabNameInput 0; }
        bind "s" { ToggleActiveSyncTab; SwitchToMode "normal"; }
        bind "Tab" { ToggleTab; }
        bind "1" { GoToTab 1; }
        bind "2" { GoToTab 2; }
        bind "3" { GoToTab 3; }
        bind "4" { GoToTab 4; }
        bind "5" { GoToTab 5; }
        bind "6" { GoToTab 6; }
        bind "7" { GoToTab 7; }
        bind "8" { GoToTab 8; }
        bind "9" { GoToTab 9; }
        bind "b" { BreakPane; SwitchToMode "normal"; }
        bind "[" { BreakPaneLeft; }
        bind "]" { BreakPaneRight; }

        bind "F3" "Enter" "Space" "Esc" { SwitchToMode "normal"; }
    }
    resize {
        bind "h" { Resize "Increase Left"; }
        bind "j" { Resize "Increase Down"; }
        bind "k" { Resize "Increase Up"; }
        bind "l" { Resize "Increase Right"; }
        bind "H" { Resize "Decrease Left"; }
        bind "J" { Resize "Decrease Down"; }
        bind "K" { Resize "Decrease Up"; }
        bind "L" { Resize "Decrease Right"; }
        bind "+" { Resize "Increase"; }
        bind "-" { Resize "Decrease"; }

        bind "F4" "Enter" "Space" "Esc" { SwitchToMode "normal"; }
    }
    move {
        bind "h" "Left" { MovePane "Left"; }
        bind "j" "Down" { MovePane "Down"; }
        bind "k" "Up" { MovePane "Up"; }
        bind "l" "Right" { MovePane "Right"; }
        bind "n" "Tab" { MovePane; }

        bind "F5" "Enter" "Space" "Esc" { SwitchToMode "normal"; }
    }
    scroll {
        bind "Ctrl e" "j" "Down" { ScrollDown; }
        bind "Ctrl y" "k" "Up" { ScrollUp; }
        bind "Ctrl f" "PageDown" "Right" "l" { PageScrollDown; }
        bind "Ctrl b" "PageUp" "Left" "h" { PageScrollUp; }
        bind "d" { HalfPageScrollDown; }
        bind "u" { HalfPageScrollUp; }
        bind "e" { EditScrollback; SwitchToMode "normal"; }
        bind "s" { SwitchToMode "entersearch"; SearchInput 0; }
        bind "Ctrl c" { ScrollToBottom; SwitchToMode "normal"; }
        bind "G" { ScrollToBottom; }

        bind "F6" "Space" "Enter" "Esc" { SwitchToMode "normal"; }
    }
    session {
        bind "d" { Detach; }
        bind "w" {
            LaunchOrFocusPlugin "zellij:session-manager" {
                floating true
                move_to_focused_tab true
            };
            SwitchToMode "normal";
        }

        bind "F7" "Enter" "Space" "Esc" { SwitchToMode "normal"; }
    }
    search {
        bind "j" "Down" { ScrollDown; }
        bind "k" "Up" { ScrollUp; }
        bind "Ctrl f" "PageDown" { PageScrollDown; }
        bind "Ctrl b" "PageUp" { PageScrollUp; }
        bind "d" { HalfPageScrollDown; }
        bind "u" { HalfPageScrollUp; }
        bind "s" { SwitchToMode "entersearch"; SearchInput 0; }
        bind "n" { Search "Down"; }
        bind "p" { Search "Up"; }
        bind "c" { SearchToggleOption "CaseSensitivity"; }
        bind "w" { SearchToggleOption "Wrap"; }
        bind "o" { SearchToggleOption "WholeWord"; }
        bind "e" { EditScrollback; SwitchToMode "normal"; }
        bind "G" { ScrollToBottom; }
        bind "Ctrl c" { ScrollToBottom; SwitchToMode "normal"; }

        bind "F6" "Space" "Enter" "Esc" { SwitchToMode "normal"; }
    }
    entersearch {
        bind "Enter" { SwitchToMode "search"; }
        bind "Ctrl c" "Esc" { SearchInput 27; SwitchToMode "scroll"; }
    }
    RenameTab {
        bind "Enter" "Ctrl c" "Esc" { SwitchToMode "tab"; }
        bind "Esc" { TabNameInput 27; SwitchToMode "tab"; }
    }
    RenamePane {
        bind "Enter" "Ctrl c" "Esc" { SwitchToMode "pane"; }
        bind "Esc" { PaneNameInput 27; SwitchToMode "pane"; }
    }
    tmux clear-defaults=true {
        bind "Ctrl c" "Enter" "Space" "Esc" { SwitchToMode "normal"; }
    }
}

// Choose what to do when zellij receives SIGTERM, SIGINT, SIGQUIT or SIGHUP
// eg. when terminal window with an active zellij session is closed
// Options:
//   - detach (Default)
//   - quit
//
// on_force_close "quit"

// Send a request for a simplified ui (without arrow fonts) to plugins
// Options:
//   - true
//   - false (Default)
//
// simplified_ui true

// Choose the path to the default shell that zellij will use for opening new panes
// Default: $SHELL
//
// default_shell "zsh"

// Toggle between having pane frames around the panes
// Options:
//   - true (default)
//   - false
//
pane_frames true

// Toggle between having Zellij lay out panes according to a predefined set of layouts whenever possible
// Options:
//   - true (default)
//   - false
//
auto_layout true

// Whether sessions should be serialized to the cache folder (including their tabs/panes, cwds and running commands) so that they can later be resurrected
// Options:
//   - true (default)
//   - false
//
session_serialization false

// Whether pane viewports are serialized along with the session, default is false
// Options:
//   - true
//   - false (default)
serialize_pane_viewport false

// Choose the theme that is specified in the themes section.
// Default: default
//
// theme "default"

// The name of the default layout to load on startup
// Default: "default"
//
// default_layout "compact"

// Choose the mode that zellij uses when starting up.
// Default: normal
//
// default_mode "locked"

// Toggle enabling the mouse mode.
// On certain configurations, or terminals this could
// potentially interfere with copying text.
// Options:
//   - true (default)
//   - false
//
// mouse_mode false

// Configure the scroll back buffer size
// This is the number of lines zellij stores for each pane in the scroll back
// buffer. Excess number of lines are discarded in a FIFO fashion.
// Valid values: positive integers
// Default value: 10000
//
scroll_buffer_size 10000

// Provide a command to execute when copying text. The text will be piped to
// the stdin of the program to perform the copy. This can be used with
// terminal emulators which do not support the OSC 52 ANSI control sequence
// that will be used by default if this option is not set.
// Examples:
//
// copy_command "xclip -selection clipboard" // x11
copy_command "wl-copy"                    // wayland
// copy_command "pbcopy"                     // osx

// Choose the destination for copied text
// Allows using the primary selection buffer (on x11/wayland) instead of the system clipboard.
// Does not apply when using copy_command.
// Options:
//   - system (default)
//   - primary
//
// copy_clipboard "primary"

// Enable or disable automatic copy (and clear) of selection when releasing mouse
// Default: true
//
copy_on_select true

// Path to the default editor to use to edit pane scrollbuffer
// Default: $EDITOR or $VISUAL
//
scrollback_editor "/usr/bin/nvim"

// When attaching to an existing session with other users,
// should the session be mirrored (true)
// or should each user have their own cursor (false)
// Default: false
//
mirror_session true

// The folder in which Zellij will look for layouts
//
// layout_dir /path/to/my/layout_dir

// The folder in which Zellij will look for themes
//
// theme_dir "/path/to/my/theme_dir"

// Enable or disable the rendering of styled and colored underlines (undercurl).
// May need to be disabled for certain unsupported terminals
// Default: true
//
styled_underlines false
