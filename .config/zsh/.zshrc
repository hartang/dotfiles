# The following lines were added by compinstall

zstyle ':completion:*' auto-description 'specify: %d'
zstyle ':completion:*' completer _expand _complete _ignored _approximate
zstyle ':completion:*' expand prefix suffix
zstyle ':completion:*' format 'Completing %d'
zstyle ':completion:*' list-colors ${(s.:.)LS_COLORS}
zstyle ':completion:*' list-prompt %SAt %p: Hit TAB for more, or the character to insert%s
zstyle ':completion:*' list-suffixes true
zstyle ':completion:*' matcher-list '' 'm:{[:lower:]}={[:upper:]}' 'r:|[._-]=** r:|=**'
zstyle ':completion:*' max-errors 2 numeric
zstyle ':completion:*' menu select=1
zstyle ':completion:*' select-prompt %SScrolling active: current selection at %p%s
zstyle ':completion:*' verbose true
zstyle :compinstall filename "$HOME/.config/zsh/.zshrc"

## fpath, must be done before compinit
fpath+=$HOME/.local/share/zsh_completions
HOST_COMPLETIONS="/run/host/usr/share/zsh/site-functions"
[[ -d "$HOST_COMPLETIONS" ]] && fpath+="$HOST_COMPLETIONS"

autoload -Uz compinit
compinit -u
# End of lines added by compinstall
# Lines configured by zsh-newuser-install
HISTFILE="$HOME/.config/zsh/.histfile"
HISTSIZE=50
SAVEHIST=50
setopt notify
unsetopt beep
bindkey -v
# End of lines configured by zsh-newuser-install
#

## Load profile
source "$HOME/.config/shell-profile/profile.sh"

## Directory shortcuts
CV="$HOME/.local/share/containers/storage/volumes"

## Sick tools
#
# Install with: cargo install --locked sheldon
export SHELDON_CONFIG_FILE="$HOME/.config/sheldon/plugins.toml"
__exists -v "sheldon" && eval "$(sheldon source)"

## Disable goodies from 'vte-profile'
#
# This is a utility that adds `/etc/profile.d/vte.sh` which installs a bunch of
# pre-cmd hooks and the like. I don't want it, but it's used by Gnome Terminal.
# So we disable it manually here.
add-zsh-hook -d precmd __vte_osc7

# CTRL-R - Paste selected command from history into commandline
skim_history_widget() {
    local selected num
    setopt localoptions noglobsubst noposixbuiltins pipefail no_aliases 2>/dev/null
    selected=( $(fc -rl 1 | awk '{ cmd=$0; sub(/^[ \t]*[0-9]+\**[ \t]+/, "", cmd); if (!seen[cmd]++) print $0 }' |
        eval sk --height "40%" ${SKIM_DEFAULT_OPTIONS-} --nth "2..,.." --bind="ctrl-r:toggle-sort,ctrl-z:ignore") )
    local ret=$?
    if [ -n "$selected" ]; then
        num=$selected[1]
        if [ -n "$num" ]; then
            zle vi-fetch-history -n "$num"
        fi
    fi
    zle reset-prompt
    return "$ret"
}
if __exists "sk"; then
    zle -N skim_history_widget
    bindkey -M vicmd '^R' skim_history_widget
    bindkey -M viins '^R' skim_history_widget
fi

