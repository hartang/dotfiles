version = '0.21.3'

-- LOCAL FUNCTIONS -------------------------------------------------------------
local function fg_color(idx)
    return { fg = { Indexed = idx } }
end

local function fg_color_mod(idx, mod)
    return {
        fg = { Indexed = idx },
        add_modifiers = mod,
    }
end

-- PLUGINS 'N' STUFF -----------------------------------------------------------
local home = os.getenv("HOME")
local xpm_path = home .. "/.local/share/xplr/dtomvan/xpm.xplr"
local xpm_repo = "https://github.com/dtomvan/xpm.xplr"
package.path = ""
    .. xpm_path .. "/?.lua;"
    .. xpm_path .. "/?/init.lua;"
    .. package.path

os.execute(
    string.format(
        "[[ -d '%s' ]] || git clone '%s' '%s'",
        xpm_path, xpm_repo, xpm_path
    )
)

require("xpm").setup({
    -- let xpm manage itself
    'dtomvan/xpm.xplr',

    'gitlab:hartan/web-devicons.xplr',
})
xplr.config.modes.builtin.default.key_bindings.on_key["P"] = {
    help = "plugins",
    messages = {
        "PopMode",
        { SwitchModeCustom = "xpm" },
    }
}

-- INPUT PROMPT ----------------------------------------------------------------
xplr.config.general.prompt.format = '❯ '
xplr.config.general.prompt.style = fg_color_mod(214, { "Bold" })

-- GENERAL CONFIG --------------------------------------------------------------
xplr.config.general.enable_mouse = false
xplr.config.general.show_hidden = false
xplr.config.general.enable_recover_mode = false

-- KEY BINDINGS ----------------------------------------------------------------
-- who uses arrow keys?!
xplr.config.modes.builtin.search.key_bindings.on_key["right"] = {}
xplr.config.modes.builtin.search.key_bindings.on_key["left"] = {}
xplr.config.modes.builtin.search.key_bindings.on_key["down"] = {}
xplr.config.modes.builtin.search.key_bindings.on_key["up"] = {}
xplr.config.modes.builtin.search.key_bindings.on_key["ctrl-h"] = {
    help = "back",
    messages = { "Back", { UpdateInputBuffer = "DeleteLine" } },
}
xplr.config.modes.builtin.search.key_bindings.on_key["ctrl-j"] = {
    help = "down",
    messages = { "FocusNext" },
}
xplr.config.modes.builtin.search.key_bindings.on_key["ctrl-k"] = {
    help = "up",
    messages = { "FocusPrevious" },
}
xplr.config.modes.builtin.search.key_bindings.on_key["ctrl-l"] = {
    help = "enter",
    messages = { "Enter", { UpdateInputBuffer = "DeleteLine" } },
}

-- Unbind unused defaults
local to_unbind = {
    "tab", "left", "up", "down", "right", "space", "{", "}", "page-down",
    "page-up", "ctrl-a", "ctrl-r", "f1"
}
for _, button in ipairs(to_unbind) do
    xplr.config.modes.builtin.default.key_bindings.on_key[button] = {}
end
xplr.config.modes.builtin.default.key_bindings.on_key["esc"] = {
    help = "clear selection",
    messages = { "ClearSelection" },
}
xplr.config.modes.builtin.default.key_bindings.on_key["ctrl-l"] = {
    help = "refresh screen",
    messages = { "Refresh" },
}
xplr.config.modes.builtin.default.key_bindings.on_key["ctrl-d"] = {
    help = "half pg down",
    messages = { "ScrollDownHalf" },
}
xplr.config.modes.builtin.default.key_bindings.on_key["ctrl-f"] = {
    help = "pg down",
    messages = { "ScrollDown" },
}
-- TODO: Remap "clear selection"
xplr.config.modes.builtin.default.key_bindings.on_key["ctrl-u"] = {
    help = "half pg up",
    messages = { "ScrollUpHalf" },
}
xplr.config.modes.builtin.default.key_bindings.on_key["ctrl-b"] = {
    help = "pg up",
    messages = { "ScrollUp" },
}
xplr.config.modes.builtin.default.key_bindings.on_key["space"] =
    xplr.config.modes.builtin.default.key_bindings.on_key[":"] -- action key bindings
xplr.config.modes.builtin.default.key_bindings.on_key[":"] = {}
xplr.config.modes.builtin.default.key_bindings.on_key["i"] = {
    help = "to pager (less)",
    messages = {
        {
            BashExec = [===[
                if command -v "less" &>/dev/null; then
                    less "$XPLR_FOCUS_PATH"
                else
                    read -p "pager 'less' not found, press enter to continue..."
                fi
            ]===]
        },
        "PopMode",
    }
}
xplr.config.modes.builtin.default.key_bindings.on_key["I"] = {
    help = "to pager (bat)",
    messages = {
        {
            BashExec = [===[
                if command -v "bat" &>/dev/null; then
                    bat "$XPLR_FOCUS_PATH"
                else
                    read -p "pager 'bat' not found, press enter to continue..."
                fi
            ]===]
        },
        "PopMode",
    }
}
xplr.config.modes.builtin.default.key_bindings.on_key["x"] = {
    help = "xdg-open",
    messages = {
        {
            BashExecSilently0 = [===[ xdg-open "${XPLR_FOCUS_PATH:?}" ]===],
        },
    },
}
xplr.config.modes.builtin.default.key_bindings.on_key["y"] = {
    help = "yank",
    messages = {
        {
            BashExecSilently0 = [===[ wl-copy < "${XPLR_FOCUS_PATH:?}" ]===],
        },
    },
}
xplr.config.modes.builtin.action.key_bindings.on_key["F"] = {
    help = "search",
    messages = {
        {
            BashExec = [===[
                PTH="$(sk)"
                if [[ -z "$PTH" ]]; then
                    exit 0
                elif [[ -d "$PTH" ]]; then
                    "$XPLR" -m 'ChangeDirectory: %q' "$PTH"
                else
                    "$XPLR" -m 'FocusPath: %q' "$PTH"
                fi
            ]===]
        },
        "PopMode",
    },
}
xplr.config.modes.builtin.action.key_bindings.on_key["t"] = {
    help = "stat",
    messages = {
        {
            BashExec = [===[
                # clear style
                CC="\e[0m"
                # heading style
                HD="\e[1m\e[4m"
                # item style
                IT="\e[1m"
                TIME="\e[38;5;220m"
                ITPATH="\e[38;5;214m"
                ITNUM="\e[38;5;135m"
                ITSTR="\e[38;5;71m"

                {
                    stat -L "$XPLR_FOCUS_PATH" --printf "${HD}General:${CC}
  ${IT}name${CC}  ${ITPATH}%N${CC}
  ${IT}size${CC}  ${ITNUM}%s${CC}
  ${IT}owner${CC}  ${ITSTR}%U${CC}:${ITSTR}%G${CC} (${ITNUM}%u${CC}:${ITNUM}%g${CC})
  ${IT}permission${CC}  ${ITSTR}%A${CC} (${ITNUM}%04a${CC})
  ${IT}selinux${CC}  ${ITSTR}%C${CC}

${HD}Time:${CC}
  ${IT}access${CC}  ${TIME}%x${CC} (%X)
  ${IT}modify${CC}  ${TIME}%y${CC} (%Y)
  ${IT}change${CC}  ${TIME}%z${CC} (%Z)
  ${IT}birth${CC}   ${TIME}%w${CC} (%W)

${HD}Raw:${CC}
  ${IT}mountpoint${CC}  ${ITPATH}%m${CC}
  ${IT}opt i/o${CC}  ${ITNUM}%-5o${CC}  ${IT}links${CC}  ${ITNUM}%-5h${CC}  ${IT}inode${CC}  ${ITNUM}%i${CC}
  ${IT}device${CC}  ${ITNUM}%d${CC} (${ITNUM}%D${CC})
    ${IT}type${CC}  ${ITNUM}%Hr${CC}:${ITNUM}%Lr${CC}
    ${IT}number${CC}  ${ITNUM}%Hd${CC}:${ITNUM}%Ld${CC}
                "

                    if [[ -d "${XPLR_FOCUS_PATH}" ]]; then
                        echo -e "\n${HD}Folder:${CC}"
                        size="$(du -shxD "${XPLR_FOCUS_PATH}" 2>/dev/null)"
                        size_exit="$?"
                        size="$(cut -f1 <<< "$size")"
                        [[ "$size_exit" -ne 0 ]] && size="$size ${ITSTR}(!)${CC}"
                        echo -e "  ${IT}size${CC}  ${ITNUM}${size}${CC}"
                    fi
                } | less -Ps"stat for '${XPLR_FOCUS_PATH}'" -c
            ]===]
        },
        "PopMode",
    },
}
xplr.config.modes.builtin.action.key_bindings.on_key["g"] = {
    help = "git info",
    messages = {
        {
            BashExec = [===[
                if [[ -d "$XPLR_FOCUS_PATH" ]]; then
                    cd "$XPLR_FOCUS_PATH"
                fi
                {
                    if ! command -v "git" &>/dev/null; then
                        echo -e "\e[31m'git' isn't installed, cannot continue\e[0m"
                    elif ! git rev-parse &>/dev/null; then
                        echo -e "\e[31m'$PWD' isn't a git repository, cannot continue\e[0m"
                    else
                        echo -e "\e[1m\e[4mRemotes:\e[0m\n"
                        git remote -v
                        echo -e "\n\e[1m\e[4mStatus:\e[0m\n"
                        git status -sb
                        echo -e "\n\e[1m\e[4mLocal Branches:\e[0m\n"
                        git bl --color=always
                        echo -e "\n\e[1m\e[4mRemote Branches:\e[0m\n"
                        git br --color=always
                        echo -e "\n\e[1m\e[4mStash entries:\e[0m\n"
                        git stash list --summary
                    fi
                } | less -Ps"git status for '$PWD'" -c
            ]===]
        },
        "PopMode",
    },
}
xplr.config.modes.builtin.action.key_bindings.on_key["!"] = {
    help = "shell",
    messages = {
        "PopMode",
        { LuaEvalSilently = [[
            local shell = os.getenv("SHELL") or "bash"
            print("\27]0;xplr - " .. shell .. "\7")
            return { { Call0 = { command = shell, args = { "-i" } } } }
        ]] },
        "ExplorePwdAsync",
    },
}

-- Borders
xplr.config.general.panel_ui.table = {
    borders = { "Left", "Bottom", "Top", "Right" },
    border_type = "Rounded",
    border_style = fg_color_mod(135, { "Bold" })
}
xplr.config.general.panel_ui.input_and_logs = {
    borders = { "Left", "Bottom", "Top", "Right" },
    border_type = "Rounded",
    border_style = fg_color(220),
}
xplr.config.general.panel_ui.help_menu = {
    borders = { "Left", "Bottom", "Top", "Right" },
    border_type = "Rounded",
    border_style = fg_color(71),
}
xplr.config.general.panel_ui.selection = {
    borders = { "Left", "Bottom", "Top", "Right" },
    border_type = "Rounded",
    border_style = fg_color(75),
}
xplr.config.general.panel_ui.sort_and_filter = {
    borders = { "Left", "Bottom", "Top", "Right" },
    border_type = "Rounded",
    border_style = fg_color(167),
}

-- file table
xplr.config.general.table.header.style = {
    add_modifiers = { "Italic" },
}

xplr.config.general.table.row.cols = {
    { format = "custom.table_col_0", },
    { format = "custom.table_col_1" },
    { format = "custom.table_col_2", },
    { format = "custom.table_col_3", },
}


xplr.config.node_types.file.meta.icon = xplr.util.paint("", fg_color(248))
xplr.config.node_types.directory = {
    meta = { icon = xplr.util.paint("", fg_color(75)) },
    style = fg_color(75),
}
xplr.config.node_types.symlink = {
    meta = { icon = xplr.util.paint("", fg_color(6)) },
    style = fg_color(6),
}

xplr.config.general.table.header.cols = {
    { format = " idx", style = {} },
    { format = "╭─── path", style = {} },
    { format = "perm", style = {} },
    { format = "size", style = {} },
}

--ERROR   deserialize error: unknown variant `Fixed`, expected one of `Percentage`, `Ratio`, `Length`, `LengthLessThanScreenHeight`, `LengthLessThanScreenWidth`, `LengthLessThanLayoutHeight`, `LengthLessThanLayoutWidth`, `Max`, `MaxLessThanScreenHeight`, `MaxLessThanScreenWidth`, `MaxLessThanLayoutHeight`, `MaxLessThanLayoutWidth`, `Min`, `MinLessThanScreenHeight`, `MinLessThanScreenWidth`, `MinLessThanLayoutHeight`, `MinLessThanLayoutWidth`
-- File table column constraints
xplr.config.general.table.col_widths = {
    { Length = 4 },
    -- All lengths added + 1 col for each spacer (between columns, before and
    -- after table)
    { LengthLessThanLayoutWidth = 25 },
    { Length = 10 },
    { Length = 6 },
}

-- More: https://xplr.dev/en/general-config.html
-- Renders the first column in the table
xplr.fn.custom.table_col_0 = function(m)
    local r = ""
    if m.is_focused then
        r = string.format("%4d", m.index)
        r = xplr.util.paint(r, fg_color(214))
    else
        r = string.format("%4d", m.relative_index)
        r = xplr.util.paint(r, { add_modifiers = { "Dim" } })
    end

    return r
end

-- Renders the second column in the table
xplr.fn.custom.table_col_1 = function(m)
    local nl = xplr.util.paint("\\n", { add_modifiers = { "Italic", "Dim" } })
    local r = m.tree .. m.prefix
    local style = xplr.util.lscolor(m.absolute_path)
    style = xplr.util.style_mix({ style, m.style })

    if m.meta.icon == nil then
        r = r .. ""
    else
        r = r .. m.meta.icon .. " "
    end

    local rel = m.relative_path
    if m.is_dir then
        rel = rel .. "/"
    end
    r = r .. xplr.util.paint(xplr.util.shell_escape(rel), style)

    r = r .. m.suffix .. " "

    if m.is_symlink then
        r = r .. "-> "

        if m.is_broken then
            r = r .. "×"
        else
            local symlink_path = xplr.util.shorten(m.symlink.absolute_path)
            if m.symlink.is_dir then
                symlink_path = symlink_path .. "/"
            end
            r = r .. symlink_path:gsub("\n", nl)
        end
    end

    return r
end

-- Renders the third column in the table
xplr.fn.custom.table_col_2 = function(m)
    local focused_mod = "Underlined"
    local accent = function(fg_col)
        if m.is_focused then
            return fg_color_mod(fg_col, { focused_mod })
        else
            return fg_color(fg_col)
        end
    end

    local r = xplr.util.paint("r", accent(135))
    local w = xplr.util.paint("w", accent(167))
    local x = xplr.util.paint("x", accent(214))
    local s = xplr.util.paint("s", accent(214))
    local S = xplr.util.paint("S", accent(214))
    local t = xplr.util.paint("t", accent(214))
    local T = xplr.util.paint("T", accent(214))
    local minus = xplr.util.paint("-", { add_modifiers = { "Dim" } })
    if m.is_focused then
        minus = xplr.util.paint("-", { add_modifiers = { "Dim", focused_mod } })
    end

    return xplr.util
        .permissions_rwx(m.permissions)
        :gsub("r", r)
        :gsub("w", w)
        :gsub("x", x)
        :gsub("s", s)
        :gsub("S", S)
        :gsub("t", t)
        :gsub("T", T)
        :gsub("-", minus)
end

-- Renders the fourth column in the table
xplr.fn.custom.table_col_3 = function(m)
    local r = ""
    if m.is_dir then
        r = r .. string.format("%3d", m.size)
    else
        local size = m.size
        local magnitude = 0
        while size > 999 do
            size = size / 1024
            magnitude = magnitude + 1
        end

        if size < 1 then
            size = 1
        end

        local suffixes = {
            [0] = " ",
            [1] = "k",
            [2] = "M",
            [3] = "G",
            [4] = "T",
            -- Anything beyond this is rediculous.
        }
        local suffix = suffixes[magnitude]
        if suffix == nil then
            suffix = "!"
        end

        if (size < 10) and (magnitude > 0) then
            r = r .. string.format("%2.1f %s", size, suffix)
        else
            r = r .. string.format("%3d %s", size, suffix)
        end
    end

    if m.is_focused then
        r = xplr.util.paint(r, fg_color(214))
    end

    return r
end

-- Renders the fifth column in the table
xplr.fn.custom.table_col_4 = function(m)
    return tostring(os.date("%H:%M:%S %Y", m.last_modified / 1000000000))
end
