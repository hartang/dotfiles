return {
    'nvim-lualine/lualine.nvim',
    opts = {
        options = {
            globalstatus = true,
            icons_enabled = true,
            draw_empty = false,
            theme = "material",
        },
        extensions = {
            'fugitive',
            'lazy',
        },
        sections = {
            lualine_a = { 'mode' },
            lualine_b = { 'branch', 'diff', 'diagnostics' },
            lualine_c = { {
                'filename',
                -- relative paths
                path = 1,
                shorting_target = 20,
            } },
            lualine_x = { 'encoding', 'fileformat', 'filetype' },
            lualine_y = { 'searchcount', 'selectioncount' },
            lualine_z = { 'progress', 'location' }
        },
        inactive_sections = {
            lualine_a = {},
            lualine_b = {},
            lualine_c = { 'filename' },
            lualine_x = { 'location' },
            lualine_y = {},
            lualine_z = {}
        },
    },
}
