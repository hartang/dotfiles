return {
    url = "https://gitlab.com/hartang/nvim/tabline.nvim",
    dependencies = { "nvim-web-devicons" },
    lazy = true,
    dev = false,
    event = "TabNew",
    --config = true,
    opts = {
        special_filetypes = {
            ["git"] = function(winnr, tabnr)
                local bufnr = vim.fn.tabpagebuflist(tabnr)[winnr]
                local git_type = vim.fn.getbufvar(bufnr, "fugitive_type")

                local parsedbuf = vim.fn.FugitiveParse(vim.fn.bufname(bufnr))
                local raw_gitdir = parsedbuf[2]
                local gitdir = vim.fn.fnamemodify(raw_gitdir, ":h:t")
                local commit = vim.fn.strcharpart(parsedbuf[1], 0, 8)
                local path = vim.fn.strcharpart(parsedbuf[1], 41)

                if git_type == "commit" then
                    return " " .. gitdir .. "@" .. commit .. "  "
                elseif git_type == "tree" then
                    local s = " " .. gitdir .. "@" .. commit
                    if path:len() > 0 then
                        s = s .. " [" .. path .. "]"
                    end
                    return s .. "  "
                else
                    return "git type: " .. git_type
                end
            end,
        }
    }
}
