return {
    "sindrets/diffview.nvim",
    cmd = {
        "DiffviewOpen",
        "DiffviewFileHistory",
    },
    init = function()
        vim.opt.fillchars:append({ diff = "╱" })
    end,
    keys = {
        { "<leader>i",  "",                                 desc = "+diff" },
        { "<leader>ii", "<Cmd>DiffviewOpen<CR>",            desc = "open" },
        { "<leader>ih", "<Cmd>DiffviewFileHistory<CR>",     desc = "branch history" },
        { "<leader>i%", "<Cmd>DiffviewFileHistory %<CR>",   desc = "file history" },
        { "<leader>ix", "<Cmd>DiffviewClose<CR>",           desc = "close" },
        { "<leader>ir", "<Cmd>DiffviewRefresh<CR>",         desc = "close" },
    },
    opts = {
        default_args = {
            DiffviewOpen = { "--untracked-files=no" },
        },
        enhanced_diff_hl = true,
        file_panel = {
            listing_style = "list",
        },
        icons = { -- Only applies when use_icons is true.
            folder_closed = "+",
            folder_open = "-",
        },
        signs = {
            fold_closed = "+",
            fold_open = "-",
            done = "✓",
        },
        view = {
            merge_tool = {
                layout = "diff3_mixed",
                disable_diagnostics = true,
                winbar_info = true,
            }
        }
    },
}
