-- Directory of this file
local config_dir = debug.getinfo(1, "S").source:sub(2):match("(.*/)")
-- Array of file types used for lazy loading
local filetypes = {}
-- Array of setup functions to be registered once the plugin loads
local setup_hooks = {}
-- Location of the LSP include files
local incpath = vim.fs.joinpath(config_dir, "nvim-lspconfig.d")

for name, ftype in vim.fs.dir(incpath) do
    if ftype == "file" then
        local filepath = vim.fs.joinpath(incpath, name)
        local chunk = dofile(filepath)

        if (chunk.filetypes ~= nil) and (type(chunk.filetypes) == "table") then
            vim.list_extend(filetypes, chunk.filetypes)
        else
            vim.api.nvim_notify(
                "LSP dropin '" .. name .. "' doesn't have required field 'filetypes'",
                vim.log.levels["ERROR"],
                {}
            )
        end

        if (chunk.setup ~= nil) and (type(chunk.setup) == "function") then
            vim.list_extend(setup_hooks, { chunk.setup })
        else
            vim.api.nvim_notify(
                "LSP dropin '" .. name .. "' doesn't have required function 'setup'",
                vim.log.levels["ERROR"],
                {}
            )
        end
    end
end

return {
    'neovim/nvim-lspconfig',
    ft = filetypes,
    config = function()
        vim.lsp.set_log_level('warn')
        -- Borders in LSP floating windows
        require('lspconfig.ui.windows').default_options = {
            border = "rounded",
        }
        local orig_util_open_floating_preview = vim.lsp.util.open_floating_preview
        function vim.lsp.util.open_floating_preview(contents, syntax, opts, ...)
            opts = opts or {}
            opts.border = "rounded"
            return orig_util_open_floating_preview(contents, syntax, opts, ...)
        end

        -- Display messages from LSP servers
        vim.lsp.handlers['window/showMessage'] = function(_, result, ctx)
            local client = vim.lsp.get_client_by_id(ctx.client_id)
            local lvl = ({ 'ERROR', 'WARN', 'INFO', 'DEBUG' })[result.type]
            require("notify")(result.message, lvl, {
                title = 'LSP | ' .. ((client or {}).name or "??"),
                timeout = 5000,
                keep = function()
                    return lvl == 'ERROR' or lvl == 'WARN'
                end,
            })
        end

        --- Setup when attaching LSP to a buffer.
        ---
        --- See vim.lsp.ClientConfig
        ---
        --- @param lsp_client vim.lsp.Client
        --- @param bufnr integer
        local on_attach = function(lsp_client, bufnr)
            if lsp_client.supports_method("textDocument/completion") then
                -- Enable completion triggered by <c-x><c-o>
                vim.bo.omnifunc = "v:lua.vim.lsp.omnifunc"
            end

            if lsp_client.supports_method("textDocument/documentHighlight") then
                -- Highlight the symbol under the cursor (and related occurrences)
                vim.api.nvim_create_autocmd({ "CursorHold", "CursorHoldI" }, {
                    buffer = bufnr,
                    desc = "highlight word under cursor on inactivity",
                    callback = function(_)
                        vim.lsp.buf.document_highlight()
                    end,
                })
                -- Clear the highlight when the cursor moves
                vim.api.nvim_create_autocmd("CursorMoved", {
                    buffer = bufnr,
                    desc = "clear cursor highlights on activity",
                    callback = function(_)
                        vim.lsp.buf.clear_references()
                    end,
                })
            end

            -- Mappings
            require("which-key").add({
                -- Options
                remap = false,
                nowait = false,
                silent = true,
                mode = "n",
                buffer = bufnr,
                -- Mappings
                { "<leader>l", group = "LSP" },
                {
                    "<leader>la",
                    vim.lsp.buf.code_action,
                    desc = "code action",
                    cond = lsp_client.supports_method("textDocument/codeAction"),
                },
                {
                    "<leader>ld",
                    vim.lsp.buf.definition,
                    desc = "go to definition",
                    cond = lsp_client.supports_method("textDocument/definition"),
                },
                {
                    "<leader>lD",
                    vim.lsp.buf.declaration,
                    desc = "go to declaration",
                    cond = lsp_client.supports_method("textDocument/declaration"),
                },
                {
                    "<leader>lf",
                    function()
                        vim.lsp.buf.format({ async = false })
                        -- Update folds and make sure the cursor fold is
                        -- visible
                        -- NOTE: We do not use `mkview`/`loadview` here because
                        -- formatting will likely change the buffer content,
                        -- messing up whatever `mkview` thought the file looks
                        -- like.
                        -- TODO(hartan): Make formatting async again, probably
                        -- register a oneshot autocmd to restore folds (but
                        -- find out what event we can trigger that on...)
                        vim.api.nvim_feedkeys("zx", "nt", true)
                    end,
                    desc = "formatting",
                    cond = lsp_client.supports_method("textDocument/formatting"),
                },
                {
                    "<leader>lf",
                    function()
                        vim.lsp.buf.format({ async = false })
                        -- Update folds and make sure the cursor fold is
                        -- visible
                        vim.api.nvim_feedkeys("zx", "nt", true)
                    end,
                    desc = "formatting",
                    mode = "v",
                    cond = lsp_client.supports_method("textDocument/rangeFormatting"),
                },
                {
                    "<leader>lh",
                    vim.lsp.buf.hover,
                    desc = "hover",
                    cond = lsp_client.supports_method("textDocument/hover"),
                },
                {
                    "<leader>lH",
                    function()
                        local is_on = vim.lsp.inlay_hint.is_enabled()
                        vim.lsp.inlay_hint.enable(not is_on)
                    end,
                    desc = "toggle inlay hints",
                    cond = lsp_client.supports_method("textDocument/inlayHint"),
                },
                {
                    "<leader>li",
                    vim.lsp.buf.implementation,
                    desc = "show implementation",
                    cond = lsp_client.supports_method("textDocument/implementation"),
                },
                {
                    "<leader>lp",
                    function() vim.lsp.buf.typehierarchy("subtypes") end,
                    desc = "subtypes",
                    cond = lsp_client.supports_method("typeHierarchy/subtypes"),
                },
                {
                    "<leader>lP",
                    function() vim.lsp.buf.typehierarchy("supertypes") end,
                    desc = "supertypes",
                    cond = lsp_client.supports_method("typeHierarchy/supertypes"),
                },
                {
                    "<leader>lr",
                    vim.lsp.buf.rename,
                    desc = "rename",
                    cond = lsp_client.supports_method("textDocument/rename"),
                },
                {
                    "<leader>lR",
                    vim.lsp.buf.references,
                    desc = "references",
                    cond = lsp_client.supports_method("textDocument/references"),
                },
                {
                    "<leader>ls",
                    vim.lsp.buf.signature_help,
                    desc = "signature",
                    cond = lsp_client.supports_method("textDocument/signatureHelp"),
                },
                {
                    "<leader>lt",
                    vim.lsp.buf.type_definition,
                    desc = "type definition",
                    cond = lsp_client.supports_method("textDocument/typeDefinition"),
                },
                {
                    "<leader>ly",
                    vim.lsp.buf.document_symbol,
                    desc = "symbols (buffer)",
                    cond = lsp_client.supports_method("textDocument/documentSymbol"),
                },
                {
                    "<leader>lY",
                    vim.lsp.buf.workspace_symbol,
                    desc = "symbols (global)",
                    cond = lsp_client.supports_method("workspace/symbol"),
                },
                {
                    -- LSP server interaction
                    { "<leader>l?",  group = "server commands" },
                    { "<leader>l?i", ":LspInfo<CR>",           desc = "info" },
                    { "<leader>l?l", ":LspLog<CR>",            desc = "log" },
                    { "<leader>l?r", ":LspRestart<CR>",        desc = "restart" },
                    { "<leader>l?s", ":LspStart<CR>",          desc = "start" },
                    { "<leader>l?k", ":LspStop<CR>",           desc = "stop" },
                },
            })
        end

        for _, hook in ipairs(setup_hooks) do
            hook({
                on_attach = on_attach
            })
        end
    end,
}
