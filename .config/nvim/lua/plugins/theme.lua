return {
    'marko-cerovac/material.nvim',
    priority = 1000,
    lazy = false,
    config = function()
        vim.g.material_style = "darker"
        require("material").setup({
            contrast = {
                floating_windows = true,
                non_current_windows = true,
            },
            styles = {
                comments = { italic = true },
                functions = { bold = true },
            },
            plugins = {
                "which-key",
                "telescope",
                "fidget",
            },
            disable = {
                -- don't override terminal colors, please
                term_colors = true,
            },
            -- Use less poppy lualine theme
            lualine_style = "stealth",
            custom_colors = function(theme_colors)
                theme_colors.editor.border = theme_colors.main.green
                if vim.g.material_style == "darker" then
                    theme_colors.editor.accent = "#ffaf00"
                end
            end,
            custom_highlights = {
                -- Popup menu (C-x, _)
                Pmenu = function(col, _)
                    return {
                        fg = col.main.yellow,
                        bg = col.backgrounds.floating_windows
                    }
                end,
                FloatTitle = function(col, hl)
                    return vim.tbl_extend(
                        "force",
                        hl.main_highlights.editor()["Title"],
                        { bg = hl.async_highlights.editor()["FloatBorder"].bg }
                    )
                end,
                TabLine = function(col, _)
                    return {
                        fg = col.main.gray,
                        bg = col.editor.bg,
                        italic = true,
                    }
                end,
                TabLineSel = function(col, _)
                    return {
                        fg = col.editor.bg,
                        bg = col.editor.accent,
                        bold = true,
                    }
                end,
                ColorColumn = function(_, _)
                    if vim.g.material_style == "darker" then
                        return { bg = "#533333" }
                    else
                        return { bg = "#ECCCCC" }
                    end
                end,
                TrailingWhitespace = function(col, _)
                    return {
                        bg = col.main.darkred,
                    }
                end,
                DiffAdd = function(col, _)
                    return {
                        fg = col.git.added,
                        bg = col.editor.active,
                        -- FIXME(ahartmann): I'd really like to play with
                        -- underlines/underdots & friends here, but my current setup
                        -- (with Zellij) doesn't support this. Vanilla wezterm and
                        -- Gnome VTE do, though, but not when used with zellij. I
                        -- assume this may have something to do with the longstanding
                        -- 0-width unicode char discard in zellij...
                        --undercurl = true,
                    }
                end,
                DiffChange = function(col, _)
                    return {
                        fg = col.git.modified,
                        bg = col.editor.active,
                    }
                end,
                DiffText = function(col, _)
                    return {
                        fg = col.git.modified,
                        bg = col.editor.active,
                    }
                end,
                DiffDelete = function(col, _)
                    return {
                        fg = col.git.removed,
                        bg = col.editor.active,
                    }
                end,
                ["@comment.todo"] = function(col, _)
                    return {
                        fg = col.syntax.comments,
                        bg = col.lsp.hint,
                    }
                end,
            },
        })
        -- Highlight trailing whitespace
        vim.api.nvim_create_autocmd(
            { "WinEnter", "VimEnter" },
            {
                callback = function(_)
                    vim.fn.matchadd("TrailingWhitespace", "\\s\\+$")
                end
            }
        )
        -- Apply theme
        vim.cmd([[colorscheme material]])
    end
}
