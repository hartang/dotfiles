return {
    'nvim-treesitter/nvim-treesitter',
    lazy = false,
    build = ':TSUpdate',
    main = "nvim-treesitter.configs",
    opts = {
        ensure_installed = {
            -- Programming languages
            "bash", "jq", "julia", "lua", "rust", "typst",
            -- File formats
            "csv", "dockerfile", "editorconfig", "json", "luadoc", "make",
            "readline", "ssh_config", "toml", "udev", "yaml",
            -- git, there is so much stuff for git...
            "git_config", "git_rebase", "gitcommit", "gitignore",
            -- Markups
            "markdown", "markdown_inline", "printf", "regex",
            -- Virtually everything injects this into comments
            "comment",
            -- Tree sitter specials
            "query"
        },
        sync_install = false,
        auto_install = false,
        highlight = {
            enable = true,
        },
    },
}
