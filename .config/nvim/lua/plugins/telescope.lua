return {
    'nvim-telescope/telescope.nvim',
    -- This is not a mistake, the branch is updated regularly
    branch = '0.1.x',
    dependencies = { 'nvim-lua/plenary.nvim' },
    cmd = "Telescope",
    keys = {
        { "<leader>t",  "<Cmd>Telescope<CR>",                       desc = "telescope" },
        { "<leader>f",  "",                                         desc = "+find" },
        { "<leader>fb", "<Cmd>Telescope buffers<CR>",               desc = "buffer" },
        { "<leader>fc", "<Cmd>Telescope colorscheme<CR>",           desc = "colorscheme" },
        { "<leader>ff", "<Cmd>Telescope find_files<CR>",            desc = "files" },
        { "<leader>fg", "<Cmd>Telescope live_grep<CR>",             desc = "grep" },
        { "<leader>fh", "<Cmd>Telescope help_tags<CR>",             desc = "help" },
        { "<leader>fl", "<Cmd>Telescope lsp_workspace_symbols<CR>", desc = "lsp (global)" },
        { "<leader>fL", "<Cmd>Telescope lsp_document_symbols<CR>",  desc = "lsp (buffer)" },
        { "<leader>fr", "<Cmd>Telescope resume<CR>",                desc = "resume prev." },
        { "<leader>ft", "<Cmd>Telescope treesitter<CR>",            desc = "tokens" },
    },
    init = function(_)
        -- See: https://github.com/nvim-telescope/telescope.nvim/issues/991
        vim.api.nvim_create_autocmd(
            { "FileType" },
            {
                pattern = "TelescopeResults",
                callback = function(args)
                    vim.o.foldlevelstart = 999
                end
            }
        )
    end,
    opts = {
        defaults = {
            -- default key bindings
            mappings = {
                -- insert mode
                i = {
                    ["<C-j>"] = function(...)
                        require("telescope.actions").move_selection_next(...)
                    end,
                    ["<C-k>"] = function(...)
                        require("telescope.actions").move_selection_previous(...)
                    end,
                },
            },
            --layout_strategy = "vertical",
            --layout_config = { height = 0.9, }
            layout_strategy = "flex",
            layout_config = {
                -- Move to horizontal mode when this many columns or more
                flip_columns = 140,
                -- Move to vertical mode when this many lines or more
                flip_lines = 30,
                -- Options for horizontal mode
                horizontal = {
                    preview_width = function(_, cols)
                        return cols - 70
                    end,
                    preview_cutoff = 100,
                },
                -- Options for vertical mode
                vertical = {
                    preview_cutoff = 40
                },
            },
            cache_picker = {
                num_pickers = 1,
                limit_entries = 1000,
            },
            history = false,
        },
        pickers = {
            find_files = { hidden = true, },
        },
    },
}
