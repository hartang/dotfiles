return {
    "saecki/crates.nvim",
    lazy = true,
    event = { "BufRead Cargo.toml" },
    opts = {
        autoload = true,
        insert_closing_quote = true,
        popup = {
            autofocus = true,
            border = "rounded",
            show_version_date = true,
            show_dependency_version = true,
            min_width = 60,
            keys = {
                select = { "<C-y>", "<CR>" },
                select_alt = { "<C-e>", "<Tab>" },
                toggle_feature = { "<CR>", "<Space>" },
            },
        },
        lsp = {
            enabled = true,
            on_attach = function(_, bufnr)
                vim.api.nvim_buf_set_option(bufnr, "omnifunc", "v:lua.vim.lsp.omnifunc")
                local crates = require("crates")

                require("which-key").register({
                    c = {
                        name = "crates",
                        d = { function() crates.show_crate_popup() end, "details" },
                        e = { function() crates.expand_plain_crate_to_inline_table() end, "expand" },
                        f = { function() crates.show_features_popup() end, "features" },
                        g = { function() crates.focus_popup() end, "go to popup" },
                        h = { function() crates.hide_popup() end, "hide popup" },
                        o = {
                            name = "open web",
                            h = { function() crates.open_homepage() end, "homepage" },
                            r = { function() crates.open_repository() end, "repo" },
                            d = { function() crates.open_documentation() end, "docs" },
                            c = { function() crates.open_crates_io() end, "crates.io" },
                        },
                        p = { function() crates.show_dependencies_popup() end, "dependencies" },
                        s = { function() crates.show_popup() end, "show (all)" },
                        t = { function() crates.extract_crate_into_table() end, "to table" },
                        u = { function() crates.upgrade_crate() end, "upgrade" },
                        U = { function() crates.upgrade_all_crates() end, "upgrade all" },
                        v = { function() crates.show_versions_popup() end, "versions" },
                    }
                }, {
                    mode = "n",
                    prefix = "<leader>",
                    noremap = true,
                    silent = true,
                    buffer = bufnr,
                    nowait = false
                })
            end,
            actions = true,
            completion = true,
            hover = true,
        },
    },
}
