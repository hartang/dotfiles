-- Change the severity for diagnostic messages to display
-- Severity codes:
-- * 1 - ERROR
-- * 2 - WARN
-- * 3 - INFO
-- * 4 - HINT
vim.b.diag_severity = { min = 4, max = 1 }
function Set_diag_severity(severity)
    vim.b.diag_severity = { min = severity, max = 1 }

    vim.diagnostic.config({
        virtual_text = {
            severity = vim.b.diag_severity,
            source = "if_many",
            spacing = 1,
        },
        signs = {
            severity = vim.b.diag_severity,
        },
        update_in_insert = false,
        severity_sort = true,
    })
end

return {
    "folke/which-key.nvim",
    event = "VeryLazy",
    opts = {
        delay = function(ctx)
            return ctx.plugin and 0 or 500
        end,
        icons = {
            colors = true,
        },
        no_overlap = true,
        preset = "classic",
        win = {
            border = "rounded",
            row = math.huge,
        },
        spec = {
            nowait = false,
            remap = false,
            -- Leader mappings
            {
                -- Buffers
                { "<leader>b",  group = "buffer" },
                { "<leader>bn", "<Cmd>bnext<CR>",     desc = "next" },
                { "<leader>bp", "<Cmd>bprevious<CR>", desc = "previous" },
                { "<leader>bx", "<Cmd>bdelete<CR>",   desc = "close" },
                { "<leader>bX", "<Cmd>bufdo bd<CR>",  desc = "close all" },
            },
            {
                -- Diagnostics
                { "<leader>d",  group = "diagnostics" },
                { "<leader>df", "<Cmd>lua vim.diagnostic.open_float()<CR>", desc = "float" },
                { "<leader>ds", "<Cmd>lua vim.diagnostic.show()<CR>",       desc = "show" },
                { "<leader>dh", "<Cmd>lua vim.diagnostic.hide()<CR>",       desc = "hide" },
                {
                    "<leader>dp",
                    function()
                        vim.diagnostic.goto_prev({ severity = vim.b.diag_severity or nil })
                    end,
                    desc = "previous"
                },
                {
                    "<leader>dn",
                    function()
                        vim.diagnostic.goto_next({ severity = vim.b.diag_severity or nil })
                    end,
                    desc = "next"
                },
                {
                    "<leader>dl",
                    function()
                        vim.diagnostic.setloclist({ severity = vim.b.diag_severity or nil })
                    end,
                    desc = "to loclist"
                },
                {
                    "<leader>dq",
                    function()
                        vim.diagnostic.setqflist({ severity = vim.b.diag_severity or nil })
                    end,
                    desc = "to quickfix"
                },
                { "<leader>dd",  group = "severity to display" },
                { "<leader>dd1", function() Set_diag_severity(1) end, desc = "error" },
                { "<leader>dd2", function() Set_diag_severity(2) end, desc = "warning" },
                { "<leader>dd3", function() Set_diag_severity(3) end, desc = "info" },
                { "<leader>dd4", function() Set_diag_severity(4) end, desc = "hint" },
            },
            {
                -- Plugins
                { "<leader>P",  group = "Plugins" },
                { "<leader>PH", "<Cmd>Lazy home<CR>",    desc = "home menu" },
                { "<leader>PI", "<Cmd>Lazy install<CR>", desc = "install missing" },
                { "<leader>PL", "<Cmd>Lazy clean<CR>",   desc = "clean unused" },
                { "<leader>PS", "<Cmd>Lazy sync<CR>",    desc = "clean and update" },
            },
            {
                -- Quickfix
                { "<leader>q",  group = "quickfix" },
                { "<leader>qt", "<Cmd>cw<CR>",     desc = "toggle" },
                { "<leader>qs", "<Cmd>copen<CR>",  desc = "show" },
                { "<leader>qh", "<Cmd>cclose<CR>", desc = "hide" },
                { "<leader>qg", "<Cmd>.cc<CR>",    desc = "go" },
                { "<leader>qn", "<Cmd>cn<CR>",     desc = "next" },
                { "<leader>qp", "<Cmd>cp<CR>",     desc = "previous" },
            },
            {
                -- Window
                { "<leader>w",  group = "window" },
                { "<leader>wn", "<Cmd>new<CR>",       desc = "new" },
                { "<leader>wo", "<Cmd>only<CR>",      desc = "only" },
                { "<leader>ws", "<Cmd>split<CR>",     desc = "split (horiz.)" },
                { "<leader>wt", "<Cmd>tab split<CR>", desc = "split (tab)" },
                { "<leader>wv", "<Cmd>vsplit<CR>",    desc = "split (vert.)" },
            },
            {
                -- Misc
                { "<leader>x", "<Cmd>bdel<CR>",         desc = "close" },
                { "<leader>X", "<Cmd>confirm qall<CR>", desc = "close all" },
                -- Diagnostic shortcuts
                {
                    "ö",
                    function()
                        vim.diagnostic.goto_prev({ severity = vim.b.diag_severity or nil })
                    end,
                    desc = "prev diagnostic"
                },
                {
                    "ä",
                    function()
                        vim.diagnostic.goto_next({ severity = vim.b.diag_severity or nil })
                    end,
                    desc = "next diagnostic"
                },
                -- Working with tabs
                -- Move to previous/next tab
                { "<A-,>", "<Cmd>tabprevious<CR>", desc = "prev tab" },
                { "<A-.>", "<Cmd>tabnext<CR>",     desc = "next tab" },
                -- Re-order to previous/next
                { "<A->>", "<Cmd>tabmove+<CR>",    desc = "move tab right" },
                { "<A-<>", "<Cmd>tabmove-<CR>",    desc = "move tab left" },
            },
        },
    },
}
