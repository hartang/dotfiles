return {
    'rcarriga/nvim-notify',
    lazy = true,
    init = function(_)
        vim.notify = function(...)
            require("notify").notify(...)
        end
    end,
    keys = {
        { "<leader>N",  "",                         desc = "+notifications" },
        { "<leader>Ny", "<Cmd>Notifications<CR>",   desc = "history (plain)" },
        {
            "<leader>Nh",
            function()
                local telescope = require("telescope")
                telescope.load_extension("notify")
                telescope.extensions.notify.notify()
            end,
            desc = "history"
        },
        { "<leader>Nd", function() require("notify").dismiss() end, desc = "dismiss" },
        { "<leader>Np", function() require("notify").pending() end, desc = "show pending" },
    },
    opts = {
        stages = "slide",
        background_colour = "#000000",
    },
}
