return {
    filetypes = { "rust" },
    setup = function(args)
        require("lspconfig").rust_analyzer.setup({
            on_attach = args.on_attach,
            settings = {
                ["rust-analyzer"] = {
                    check = {
                        command = "clippy",
                    },
                    experimental = {
                        serverStatusNotification = true,
                    },
                },
            },
        })
    end
}
