return {
    filetypes = { "markdown" },
    setup = function(args)
        require('lspconfig').marksman.setup {
            on_attach = args.on_attach
        }
    end,
}
