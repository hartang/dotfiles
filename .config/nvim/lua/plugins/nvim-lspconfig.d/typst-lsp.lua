local filetypes = { "typst" }

return {
    filetypes = filetypes,
    setup = function(args)
        require('lspconfig').typst_lsp.setup({
            on_attach = args.on_attach,
            enabled = true,
            filetypes = filetypes,
        })
    end,
}
