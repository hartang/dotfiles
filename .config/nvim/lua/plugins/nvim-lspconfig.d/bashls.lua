return {
    filetypes = { "sh" },
    setup = function(args)
        require('lspconfig').bashls.setup({
            on_attach = args.on_attach,
            enabled = true,
            -- See: https://github.com/neovim/neovim/issues/14504#issuecomment-833940045
            before_init = function(init_params, _)
                init_params.processId = vim.NIL
            end,
            settings = {
                -- See: <https://github.com/bash-lsp/bash-language-server/blob/main/server/src/config.ts>
                bashIde = {
                    backgroundAnalysisMaxFiles = 64,
                    includeAllWorkspaceSymbols = false,
                    shellcheckArguments = { "--severity=style", "--wiki-link-count=3" },
                    -- This tool doesn't appear to have a config file format...
                    shfmt = {
                        path = "shfmt",
                        ignoreEditorconfig = false,
                        languageDialect = "auto",
                        binaryNextLine = false,
                        caseIndent = true,
                        funcNextLine = false,
                        simplifyCode = false,
                        spaceRedirects = true,
                    },
                },
            },
        })
    end,
}
