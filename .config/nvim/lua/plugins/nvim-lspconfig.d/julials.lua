return {
    filetypes = { "julia" },
    setup = function(args)
        require('lspconfig').julials.setup({
            on_attach = args.on_attach,
            enabled = true,
            filetypes = { "julia" },
            cmd = { "julia",
                "--startup-file=no",
                "--history-file=no",
                "-e",
                [==[
                    import Pkg
                    try
                        # Maybe the current env has LSP installed?
                        import LanguageServer
                        println(stderr, "using LSP from environment")
                    catch
                        has_lsp = isdir(Pkg.envdir()) && "nvim-lspconfig" in readdir(Pkg.envdir())
                        Pkg.activate("nvim-lspconfig"; shared = true)
                        if ! (has_lsp)
                            println(stderr, "performing initial LSP installation")
                            Pkg.add("LanguageServer")
                            println(stderr, "done")
                        end
                        import LanguageServer
                    end
                    Pkg.activate()

                    depot_path = get(ENV, "JULIA_DEPOT_PATH", "")
                    project_path = let
                        dirname(something(
                            ## 1. Finds an explicitly set project (JULIA_PROJECT)
                            Base.load_path_expand((
                                p = get(ENV, "JULIA_PROJECT", nothing);
                                p === nothing ? nothing : isempty(p) ? nothing : p
                            )),
                            ## 2. Look for a Project.toml file in the current working directory,
                            ##    or parent directories, with $HOME as an upper boundary
                            Base.current_project(),
                            ## 3. First entry in the load path
                            get(Base.load_path(), 1, nothing),
                            ## 4. Fallback to default global environment,
                            ##    this is more or less unreachable
                            Base.load_path_expand("@v#.#"),
                        ))
                    end

                    @info "Running language server" VERSION pwd() project_path depot_path
                    server = LanguageServer.LanguageServerInstance(stdin, stdout, project_path, depot_path)
                    server.runlinter = true
                    run(server)
                    ]==],
            },
        })
    end,
}
