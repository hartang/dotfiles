return {
    filetypes = { "yaml.ansible" },
    setup = function(args)
        require("lspconfig").ansiblels.setup({
            on_attach = args.on_attach,
            enabled = true,
            -- See: https://github.com/neovim/neovim/issues/14504#issuecomment-833940045
            before_init = function(init_params, _)
                init_params.processId = vim.NIL
            end,
        })
    end,
}
