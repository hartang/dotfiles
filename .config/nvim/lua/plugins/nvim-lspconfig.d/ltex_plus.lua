local filetypes = {
    "bib",
    "context",
    "gitcommit",
    "html",
    "mail",
    "markdown",
    "mdx",
    "org",
    "pandoc",
    "plaintex",
    "plaintext",
    "quarto",
    "rmd",
    "rnoweb",
    "rst",
    "tex",
    "text",
    "typst",
    "xhtml"
}

return {
    filetypes = filetypes,
    setup = function(args)
        require('lspconfig').ltex_plus.setup {
            on_attach = args.on_attach,
            enabled = true,
            -- Require manual start.
            --
            -- The LSP is pretty slow to parse files (since it's built entirely
            -- using regular expressions) and causes high CPU and memory
            -- consumption.
            autostart = false,
            filetypes = filetypes,
            -- I'm not using this plugin for fun, you know? Ffs...
            settings = {
                ltex = {
                    language = "de-DE",
                    enabled = filetypes,
                },
            },
        }
    end,
}
