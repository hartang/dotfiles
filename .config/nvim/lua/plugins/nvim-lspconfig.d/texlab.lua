local filetypes = { "tex", "bib", "plaintex", "latex" }

return {
    filetypes = filetypes,
    setup = function(args)
        require('lspconfig').texlab.setup {
            on_attach = args.on_attach,
            -- Override root directory when config changes
            on_new_config = function(new_config, new_root_dir)
                new_config.settings.texlab.rootDirectory = new_root_dir
            end,
            filetypes = filetypes,
            settings = {
                texlab = {
                    bibtexFormatter = "texlab",
                    build = {
                        args = { "-X", "compile", "%f", "--synctex", "--keep-logs", "--keep-intermediates" },
                        executable = "tectonic",
                        forwardSearchAfter = false,
                        onSave = true,
                    },
                    chktex = {
                        onEdit = true,
                        onOpenAndSave = true,
                    },
                    diagnosticsDelay = 300,
                    -- Bibtex only
                    formatterLineLength = 80,
                    latexFormatter = "latexindent",
                    latexindent = {
                        modifyLineBreaks = true,
                    },
                }
            },
        }
    end,
}
