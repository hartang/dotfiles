return {
    filetypes = { "lua" },
    setup = function(args)
        require('lspconfig').lua_ls.setup({
            on_attach = args.on_attach,
            -- LSP extensions for nvim
            on_init = function(client)
                client.config.settings.Lua = vim.tbl_deep_extend('force', client.config.settings.Lua, {
                    runtime = {
                        -- Tell the language server which version of Lua you're using
                        -- (most likely LuaJIT in the case of Neovim)
                        version = 'LuaJIT'
                    },
                    -- Make the server aware of Neovim runtime files
                    workspace = {
                        checkThirdParty = false,
                        library = {
                            vim.env.VIMRUNTIME
                            -- "${3rd}/luv/library"
                        }
                    }
                })
            end,
            settings = { Lua = {} },
        })
    end,
}
