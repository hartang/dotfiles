return {
    'tpope/vim-fugitive',
    cmd = "Git",
    keys = {
        { "<leader>g",      "",                                     desc = "+git" },
        { "<leader>gb",     "",                                     desc = "+branch" },
        { "<leader>gbc",    "<Cmd>Git branch --show-current<CR>",   desc = "current" },
        { "<leader>gbl",    "<Cmd>Git branch<CR>",                  desc = "local" },
        { "<leader>gbr",    "<Cmd>Git branch --remote<CR>",         desc = "remote" },
        { "<leader>gbs",    "<Cmd>Git show-branch --current --topo-order --sha1-name", desc = "show" },

        { "<leader>gc",     "",                                     desc = "+commit" },
        { "<leader>gc<CR>", "<Cmd>Git commit<CR>",                  desc = "commit" },
        { "<leader>gca",    "<Cmd>Git commit --amend<CR>",          desc = "amend" },
        { "<leader>gcc",    "<Cmd>Git rev-parse --short=8 HEAD<CR>", desc = "current" },

        { "<leader>gd",     "",                                     desc = "+diff" },
        { "<leader>gd<CR>", "<Cmd>Git diff<CR>",                    desc = "diff" },
        { "<leader>gdc",    "<Cmd>Git diff --cached<CR>",           desc = "diff cached" },
        { "<leader>gds",    "<Cmd>Gdiffsplit<CR>",                  desc = "split diff" },

        { "<leader>gf",     "",                                     desc = "+fetch" },
        { "<leader>gf<CR>", "<Cmd>Git fetch<CR>",                   desc = "fetch" },
        { "<leader>gfp",    "<Cmd>Git fetch --prune<CR>",           desc = "prune" },

        { "<leader>gg",     "<Cmd>tab Git<CR>",                     desc = "overview" },

        { "<leader>gl",     "",                                     desc = "+log" },
        { "<leader>gl<CR>", "<Cmd>Git log<CR>",                     desc = "log" },
        { "<leader>glg",    "<Cmd>Git log --graph<CR>",             desc = "graph" },
        { "<leader>gl1",    "<Cmd>Git log --graph --oneline<CR>",   desc = "oneline" },
        { "<leader>gld",    "<Cmd>Git whatchanged -p --graph<CR>",  desc = "diff" },

        { "<leader>gp",     "",                                     desc = "+push" },
        { "<leader>gp<CR>", "<Cmd>Git push<CR>",                    desc = "push" },
        { "<leader>gpf",    "<Cmd>Git push --force-with-lease<CR>", desc = "force+lease" },
        { "<leader>gpF",    "<Cmd>Git push --force<CR>",            desc = "force!" },
        { "<leader>gpu",    "<Cmd>Git push -u origin ",             desc = "upstream" },

        { "<leader>gr",     "",                                     desc = "+rebase" },
        { "<leader>gra",    "<Cmd>Git rebase --abort<CR>",          desc = "abort" },
        { "<leader>grc",    "<Cmd>Git rebase --continue<CR>",       desc = "continue" },
        { "<leader>gru",    "<Cmd>Git rebase HEAD@{u}<CR>",         desc = "upstream" },

        { "<leader>gs",     "",                                     desc = "+status" },
        { "<leader>gs<CR>", "<Cmd>Git status<CR>",                  desc = "status" },

        { "<leader>gt",     "",                                     desc = "+stash" },
        { "<leader>gta",    "<Cmd>Git stash apply<CR>",             desc = "apply" },
        { "<leader>gtd",    "<Cmd>Git stash drop<CR>",              desc = "drop" },
        { "<leader>gto",    "<Cmd>Git stash pop<CR>",               desc = "pop" },
        { "<leader>gtu",    "<Cmd>Git stash push<CR>",              desc = "push" },

        { "<leader>gu",     "",                                     desc = "+pull" },
        { "<leader>gu<CR>", "<Cmd>Git pull<CR>",                    desc = "pull" },
        { "<leader>guR",    "<Cmd>Git pull --rebase<CR>",           desc = "rebase" },
    }
}
