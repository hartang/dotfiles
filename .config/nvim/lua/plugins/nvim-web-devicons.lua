return {
    'nvim-tree/nvim-web-devicons',
    lazy = true,
    opts = {
        color_icons = true,
        strict = true,
    },
}
