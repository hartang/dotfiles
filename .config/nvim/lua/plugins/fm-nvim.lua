return {
    "is0n/fm-nvim",
    opts = {
        ui = {
            default = "float",
            float = {
                border = "rounded",
                float_hl = "Normal",
                border_hl = "FloatBorder",
            }
        },
        mappings = {
            vert_split = "<C-v>",
            horz_split = "<C-h>",
            tabedit    = "<C-t>",
            edit       = "<C-e>",
            ESC        = "<ESC>"
        },
    },
    keys = {
        { "<leader><CR>", "<Cmd>Xplr<CR>", desc = "xplr" },
    }
}
