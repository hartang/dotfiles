-- nvim_open_win opens floating windows, need to override frames there
-- Here's an issue discussing this: https://github.com/neovim/neovim/issues/20202
-- vim.lsp.util.get_progress_messages() might be handy with my notification plugin

-- Define leader key to avoid mapping issues with lazy.nvim
vim.g.mapleader = " "
vim.g.maplocalleader = " "
vim.opt.timeoutlen = 500
vim.opt.updatetime = 2000

-- Load plugins
local lazypath = vim.fn.stdpath("data") .. "/lazy/lazy.nvim"
if not vim.loop.fs_stat(lazypath) then
    vim.fn.system({
        "git",
        "clone",
        "--filter=blob:none",
        "https://github.com/folke/lazy.nvim.git",
        "--branch=stable", -- latest stable release
        lazypath,
    })
end
vim.opt.rtp:prepend(lazypath)
require("lazy").setup("plugins", {
    dev = {
        -- Resolve local paths for plugins marked with 'dev = true'
        path = function(plugin)
            if plugin[1] then
                return "~/repos/github.com/" .. plugin[1]
            elseif plugin.url then
                return plugin.url:gsub("^https://", "~/repos/")
            elseif plugin.dir then
                return plugin.dir
            else
                vim.api.nvim_err_writeln(
                    "failed to determine dev-directory for plugin:\n" ..
                    vim.inspect(plugin)
                )
                return "/tmp"
            end
        end,
    },
    install = {
        missing = true,
    },
    ui = {
        border = "rounded",
        title = " Plugins ",
    },
    performance = {
        cache = {
            enabled = true
        },
    },
})

-- === GENERAL VIM SETTINGS ===
-- Tabs become 4 spaces
vim.opt.expandtab = true
vim.opt.tabstop = 4
vim.opt.shiftwidth = 4
-- Highlight current line
vim.opt.cursorline = true
vim.opt.cursorcolumn = true
-- Show line numbers
vim.opt.number = true
vim.opt.relativenumber = true
vim.opt.numberwidth = 4
-- Vertical rulers after columns 80 and 100
vim.opt.colorcolumn = "81,101"
-- Disable swap file
vim.opt.swapfile = false
-- Split windows to the right and down
vim.opt.splitbelow = true
vim.opt.splitright = true
-- Handle folds
vim.opt.foldmethod = "expr"
vim.opt.foldexpr = "v:lua.vim.treesitter.foldexpr()"
vim.opt.foldopen = "hor,mark,percent,quickfix,search,tag"
vim.opt.foldcolumn = "auto:4"
vim.opt.foldlevelstart = 0
vim.opt.foldminlines = 1
vim.opt.foldtext=""
vim.opt.fillchars:append({ fold = "<" })
-- Smooth scrolling for long wrapped lines
vim.opt.smoothscroll = true
-- Disable all mouse handling
vim.opt.mouse = ""
vim.opt.mousefocus = false
vim.opt.mousemoveevent = false
vim.opt.mousescroll = "ver:0,hor:0"
