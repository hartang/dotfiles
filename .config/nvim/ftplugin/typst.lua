if vim.b.did_ftplugin then
    return
end
vim.b.did_ftplugin = true

-- For inspiration, have a look at e.g. '/usr/share/nvim/runtime/ftplugin/rust.vim'

vim.opt_local.comments = {
    -- Multiline comment with '*' on every intermediate line
    "s1:/*", "mb:*", "ex:*/",
    -- Multiline comment without repeating '*'
    "s0:/*", "mb: ", "ex:*/",
    -- Singleline comments
    ":///", "://!", "://"
}
vim.bo.commentstring = "//%s"
vim.opt_local.formatoptions:remove("t")
vim.opt_local.formatoptions:append("croqnlj")
