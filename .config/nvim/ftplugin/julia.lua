if vim.b.did_ftplugin then
    return
end
vim.b.did_ftplugin = true

-- This is necessary because the LSP and all formatting tools refuse to break
-- long comment lines. So we rely on the vim-builtin word formatting instead
-- (`gww`).
vim.b.textwidth=99
