# Install with: cargo install --locked zellij
ZELLIJ_SESSION_FLAG="$XDG_RUNTIME_DIR/.zellij-session"
if __exists "zellij" && [[ -t 1 ]]; then
    if [[ -n "${ZELLIJ:-""}" ]] || [[ -f "$ZELLIJ_SESSION_FLAG" ]]; then
        # We're in a zellij session already
        echo -n
    else
        # There's no zellij session running, apparently
        echo "$$" > "$ZELLIJ_SESSION_FLAG"
        if __exists "zsh"; then
            SHELL="/usr/bin/zsh" zellij a -c default
        elif __exists "toolbox" &&
            toolbox list -c | tail -n+2 | tr -s ' ' | cut -d' ' -f2 | grep -q "default" &&
            toolbox run -c "default" command -v "zsh" &>/dev/null; then
            # There is a toolbox container called "default" and it has zsh installed
            toolbox run -c "default" env SHELL="/usr/bin/zsh" zellij a -c default
        elif [[ ! -f "/run/.toolboxenv" ]]; then
            SHELL="/usr/bin/bash" zellij a -c host
        else
            echo "ERROR: don't know how to handle this case for zellij attachment" 1>&2
        fi
        # Clean up after ourselves
        if [[ "$(< "$ZELLIJ_SESSION_FLAG")" -eq "$$" ]]; then
            rm -f "$ZELLIJ_SESSION_FLAG"
        fi
    fi
fi

