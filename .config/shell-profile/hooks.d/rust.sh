# Migrate files to different path if needed
if [[ -d "$HOME/.cargo" ]] &&
    [[ -n "${CARGO_HOME:-""}" ]] &&
    [[ "$CARGO_HOME" != "$HOME/.cargo" ]]; then
    mv "$HOME/.cargo" "$CARGO_HOME"
    echo -e "\e[32mcargo installation migrated to '$CARGO_HOME'\e[0m"
fi
if [[ -d "$HOME/.rustup" ]] &&
    [[ -n "${RUSTUP_HOME:-""}" ]] &&
    [[ "$RUSTUP_HOME" != "$HOME/.rustup" ]]; then
    mv "$HOME/.rustup" "$RUSTUP_HOME"
    echo -e "\e[32mrustup installation migrated to '$RUSTUP_HOME'\e[0m"
fi
