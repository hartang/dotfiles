# Install with: cargo install --locked starship
__exists -v "starship" &&
    eval "$(starship init "$(basename "$SHELL")")"

