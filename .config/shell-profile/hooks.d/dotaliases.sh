# Migrate dotfiles/dotsecrets to new location, if present

DOTFILES_OLD_PATH="$HOME/repos/dotfiles"
DOTFILES_NEW_PATH="$HOME/repos/gitlab.com/hartang/dotfiles"

if [[ -d "$DOTFILES_OLD_PATH" ]]; then
    rm -rf "$DOTFILES_NEW_PATH"
    mkdir -p "$(dirname "$DOTFILES_NEW_PATH")"
    mv "$DOTFILES_OLD_PATH" "$DOTFILES_NEW_PATH"
    echo -e "\e[32mmigrated dotfiles to '$DOTFILES_NEW_PATH'\e[0m"
fi


DOTSECRETS_OLD_PATH="$HOME/repos/dotsecrets"
DOTSECRETS_NEW_PATH="$HOME/repos/gitlab.com/hartang/dotsecrets"

if [[ -d "$DOTSECRETS_OLD_PATH" ]]; then
    rm -rf "$DOTSECRETS_NEW_PATH"
    mkdir -p "$(dirname "$DOTSECRETS_NEW_PATH")"
    mv "$DOTSECRETS_OLD_PATH" "$DOTSECRETS_NEW_PATH"
    echo -e "\e[32mmigrated dotsecrets to '$DOTSECRETS_NEW_PATH'\e[0m"
fi
