# Check if a command exists.
#
# If the command is found, exits with status 0, otherwise exits with status 1.
# If '-v'/'--verbose' is given as additional argument, a message is shown on
# stderr to notify the user that an important command is missing and
# functionality may be degraded.
#
# Arguments:
#   $1: The command to search for
function __exists {
    local verbose=false
    local cmd=""
    while [[ $# -gt 0 ]]; do
        case "$1" in
            "-v"|"--verbose")
                verbose=true
                shift
                ;;
            *)
                cmd="$1"
                shift
                ;;
        esac
    done

    if command -v "$cmd" &>/dev/null; then
        return 0
    else
        "$verbose" &&
            echo "'$SHELL' requires '$cmd', some functionality may be missing" 1>&2
        return 1
    fi
}

PROFILE_DIR="$HOME/.config/shell-profile"

source "$PROFILE_DIR/ls_colors.sh"
source "$PROFILE_DIR/variables.sh"
source "$PROFILE_DIR/path.sh"
source "$PROFILE_DIR/aliases.sh"
source "$PROFILE_DIR/functions.sh"

for file in "$PROFILE_DIR/hooks.d/"*; do
    # shellcheck disable=SC1090
    source "$file"
done
