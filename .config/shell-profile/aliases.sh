alias clc="clear"
alias dotfiles='/usr/bin/git --git-dir=$HOME/repos/gitlab.com/hartang/dotfiles --work-tree=$HOME'
alias dotsecrets='/usr/bin/git --git-dir=$HOME/repos/gitlab.com/hartang/dotsecrets --work-tree=$HOME'
alias dud="du -achxd1 . 2>/dev/null | sort -h"
alias g="git"
alias istoolbx='[ -f "/run/.toolboxenv" ] && grep -oP "(?<=name=\")[^\";]+" /run/.containerenv'
alias sp="sk $SKIM_PREVIEW_OPTS";
alias vgit="vim -c ':Git | :wincmd o'"
alias xo="xdg-open"

# Overrides of existing tools
alias info='info --init-file "$HOME/.config/infokey"'
__exists "lsd" && alias ls="lsd"
alias lsblk='lsblk --highlight "( SIZE >= $((32*1024*1024*1024)) ) && ( TYPE != \"disk\" )"'
alias rsync="rsync --info=name0,progress2"
alias vim=nvim
