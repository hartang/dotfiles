export __SHELL_PROFILE_PATH_LOADED__=1

[[ ":${PATH}:" != *:"${CARGO_HOME:-$HOME/.cargo}/bin":* ]] &&
    export PATH="${CARGO_HOME:-$HOME/.cargo}/bin:$PATH"
[[ ":${PATH}:" != *:"$HOME/.local/bin":* ]] &&
    export PATH="$HOME/.local/bin:$PATH"

