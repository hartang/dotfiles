function ssh-fw {
    if [[ $# -eq 0 ]]; then
        echo "you must provide at least one argument" 1>&2
        return 1
    fi

    REMOTE_UID="$(id -u)"
    if [[ "$1" =~ ^[0-9]+$ ]]; then
        REMOTE_UID="$1"
        shift 1
    fi

    declare -a forward_args=(
        -o RemoteForward="/run/user/${REMOTE_UID}/gnupg/S.gpg-agent.ssh /run/user/%i/gnupg/S.gpg-agent.ssh"
        -o RemoteForward="/run/user/${REMOTE_UID}/gnupg/S.gpg-agent /run/user/%i/gnupg/S.gpg-agent.extra"
    )

    # shellcheck disable=SC2029
    ssh "${forward_args[@]}" "$@"
}

__exists -v "fd" &&
    __exists -v "sk" &&
    function cds { cd "$(fd -td -H | sk || return)" || return; }

if __exists -v "xplr"; then
    function _xplr_get_dir {
        dir="$PWD"
        if [[ $# -gt 0 ]]; then
            [[ -d "$1" ]] || { echo "'$1' is not a directory" 1>&2; return 1; }
            dir="$1"
        fi

        local -i exit_code=0
        ret="$(xplr "$dir")" || exit_code=$?

        if [[ "$exit_code" -ne 0 ]]; then
            echo "'xplr' didn't return a valid directory" 1>&2
            return 1
        elif [[ -z "$ret" ]]; then
            echo "."
        else
            echo "$ret"
        fi
    }

    function cx {
        dir="$(_xplr_get_dir "${1:-$PWD}")" &&
            cd "$dir" ||
            return 1
    }
    function px {
        dir="$(_xplr_get_dir "${1:-$PWD}")" &&
            pushd "$dir" ||
            return 1
    }
fi

# Custom shell command, see: https://gitlab.com/c8160/shell-helpers
if __exists -v "get" &&
    __exists -v "mktemp"
then
    function cg {
        local -i exit_code=0
        # FIXME(hartan): If anyone knows how to get rid of this intermediary
        # file, please tell me...
        local -r logfile="$(mktemp)"
        function _die {
            echo "$@" 1>&2
            rm -f "$logfile"
            return 1
        }

        get -p "$logfile" "$@" || exit_code=$?
        if [[ "$exit_code" -eq 0 ]]; then
            [[ -f "$logfile" ]] ||
                { _die "'get' didn't provide an output directory" 1>&2; return 1; }
            target_dir="$(< "$logfile")"
            [[ -n "$target_dir" ]] ||
                { _die "cannot determine output directory of 'get'" 1>&2; return 1; }
            cd "$target_dir" || return 1
        else
            _die "failed to fetch repo, cannot change directory" 1>&2
            return 1
        fi
        rm -f "$logfile"
    }
fi
