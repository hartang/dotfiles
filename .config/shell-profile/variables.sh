export __SHELL_PROFILE_VARIABLES_LOADED__=1

# system-wide variables
if __exists "nvim"; then
    export EDITOR=nvim
elif __exists "vim"; then
    export EDITOR=vim
elif __exists "vi"; then
    export EDITOR="vi"
else
    echo "No sensible editor found. Please install one." 1>&2
fi
LS_COLORS="$(export IFS=":" && echo "${LS_COLORS_ARRAY[*]}:")" &&
    export LS_COLORS
export PAGER="less"

# bash/readline
export INPUTRC="$HOME/.config/readline/inputrc"
export HISTFILE="$HOME/.cache/bash_history"
# See ~/.config/lesskey for additional info
declare -a LESS_OPTIONS=(
    --status-line --show-preproc-errors --line-num-width=4 --incsearch
    -x4 -W -S -i -j1 -R --use-color
    # Colors (Binary, Control, Errors, Marks, Linenumbers, Prompts, Rscroll, Highlight from -w)
    '-DB232.220$' '-DC232.71$' '-DE167.-$' '-DM135.-$' '-DN214.-$' '-DP232.75$'
    '-DW232.167$' '-DR232.75$'
    # Colors (Search results and matched parenthesized subpatterns)
    '-DS232.214$' '-D1232.167$' '-D2232.135$' '-D3232.75$' '-D4232.71$' '-D5232.220$'
    # Colors (formatted text)
    '-Dd+135.-$' '-Dk+220.-$' '-Ds+167.-$' '-Du+71.-$'
    # Custom prompts (details and short)
    '-P=?ltlines %lt-%lb/%L .?Pb(%Pb\%), .?btbytes %bt-%bb/%B .@ ?f%T "%f":<STDIN>.%t$'
    '-Ps?ltlines %lt-%lb/%L .?Pb(%Pb\%) .@ ?f%T "%f":<STDIN>.%t$'
)
export LESS="${LESS_OPTIONS[*]}"
# lynx
export LYNX_CFG_PATH="$HOME/.config/lynx"
export LYNX_CFG="$LYNX_CFG_PATH/lynx.cfg"
export LYNX_LSS="$LYNX_CFG_PATH/lynx.lss"
# GNU make
export MAKEFLAGS=""
export GNUMAKEFLAGS="w -Otarget --warn-undefined-variables"
# man
export MANPAGER="less"
export MANROFFOPT="-c"
# GNU pass
export PASSWORD_STORE_DIR="$HOME/.local/share/password-store"
export PASSWORD_STORE_ENABLE_EXTENSIONS="true"
export PASSWORD_STORE_GENERATED_LENGTH="64"
# Rust/Cargo
export CARGO_HOME="$HOME/.local/share/cargo"
export RUSTUP_HOME="$HOME/.local/share/rustup"
# skim/sk
export SKIM_DEFAULT_COMMAND='fd -tf -H';
export SKIM_DEFAULT_OPTIONS='--layout reverse --inline-info';
export SKIM_CTRL_T_COMMAND="$SKIM_DEFAULT_COMMAND";
export SKIM_CTRL_T_OPTS="$SKIM_PREVIEW_OPTIONS";
export SKIM_PREVIEW_OPTS="--preview 'bat --style numbers --color always --line-range :500 {}' --preview-window down:40%";
# ssh
export SSH_AUTH_SOCK="${XDG_RUNTIME_DIR:-/run/user/$(id -u)}/gnupg/S.gpg-agent.ssh"
# systemd
export SYSTEMD_COLORS="256"
export SYSTEMD_LESS="$LESS -F -X"
export SYSTEMD_PAGERSECURE="false"

